﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
///  Gestor de la pantalla de pausa del juego
/// </summary>
public class PauseScreenManager : MonoBehaviour {

    /// <summary>
    ///  Gameobject que representara la pantalla de pausa del juego
    /// </summary>
    public GameObject pauseScreen;

    /// <summary>
    ///  Función que vuelve al usuario a la pantalla de juego
    ///  para reiniciar así la partida
    /// </summary>
    public void RestartScene()
    {
        SceneManager.LoadScene("Mario1-1");
    }

    /// <summary>
    ///  Función que permite al usuario salir del juego
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        /*  Comprobamos que el usuario a pulsado el boton "Escape". 
            Se muestra o se oculta en consecuencia. */
        if (Input.GetKeyUp(KeyCode.Escape))
            pauseScreen.SetActive(!pauseScreen.activeSelf);
    }

}
