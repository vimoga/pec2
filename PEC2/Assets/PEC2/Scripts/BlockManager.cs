﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlador de los bloques ?. Usado para controlar 
/// la detección de colisión y mostrar el ítem obtenido.
/// </summary>
public class BlockManager : MonoBehaviour {

    /// <summary>
    /// game object de la moneda de dentro del bloque
    /// </summary>
    private GameObject coinInBlock;

    /// <summary>
    /// Controlador de las animaciones del bloque.
    /// </summary>
    private Animator myAnimator;

    /// <summary>
    /// Controlador general del juego. Usado para gestionar la partida.
    /// </summary>
    public GameplayManager gameplayManager;

    /// <summary>
    /// Indica si el bloque ya ha sido usado.
    /// </summary>
    private bool isUsed = false;

    /// <summary>
    /// veces que moveremos la moneda hacia arriba una vez se ha activado el bloque.
    /// </summary>
    private int coinElevation = 6;

    
    // Use this for initialization
    void Start () {
        myAnimator = GetComponent<Animator>();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isUsed)
        {
            if (collision.gameObject != gameObject)
            {
                //Obtenemos la dirección de la colisión.
                Vector2 direction = transform.position - collision.transform.position;

                if (collision.gameObject.tag == "MARIO")
                {

                    //si el jugador a colisionado con el bloque ? sacamos una moneda y la añadimos al contador.
                    if (direction.y > 0)
                    {
                        myAnimator.SetBool("Used", true);
                        isUsed = true;
                        gameplayManager.AddCoin();
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update () {

        //movemos la moneda que está  en el bloque hacia arriba.
        if (isUsed && coinElevation > 0) {

            Vector3 childPosition = gameObject.transform.GetChild(0).position;
            childPosition.y += 0.05f;
            gameObject.transform.GetChild(0).position = childPosition;

            coinElevation -= 1;

        } else if (isUsed && coinElevation == 0) {
            //si la moneda ya se a movido del todo la eliminamos.
            if (gameObject.transform.childCount > 0) {
                Destroy(gameObject.transform.GetChild(0).gameObject);
            }
            
        }
	}
}
