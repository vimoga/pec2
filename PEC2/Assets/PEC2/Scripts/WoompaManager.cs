﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

/// <summary>
/// Controlador de los enemigos del juego.
/// Implementara desde el movimiento a la muerte del enemigo.
/// </summary>
public class WoompaManager : MonoBehaviour {


    /// <summary>
    /// Controlador general del juego. Usado para gestionar la partida.
    /// </summary>
    public GameplayManager gameplayManager;

    /// <summary>
    /// Controlador de las físicas del cuerpo del enemigo.
    /// </summary>
    private Rigidbody2D myRigidbody;

    /// <summary>
    /// Controlador de las animaciones del enemigo.
    /// </summary>
    private Animator myAnimator;

    /// <summary>
    /// Conponente de los sonidos efectuados por el enemigo.
    /// </summary>
    private AudioSource source;

    /// <summary>
    /// Velocidad de movimiento del enemigo.
    /// </summary>
    public float linealVelocity = 4.2f;

    /// <summary>
    /// Indica si el enemigo se está dirigiendo hacia la izquierda. Usado para controlar la dirección.
    /// </summary>
    private bool isFacingLeft = true;

    // Use this for initialization
    void Start () {

        myRigidbody = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();

        myAnimator.SetBool("Dead", false);

        source = GetComponent<AudioSource>();

        //evita rotaciones incesarias
        myRigidbody.freezeRotation = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject != gameObject)
        {

            //Obtenemos la dirección de la colisión.
            Vector2 direction = transform.position - collision.transform.position;

            if ((collision.gameObject.tag == "SCENARY" || collision.gameObject.tag == "WOOMPA") && direction.x != 0)
            {
                //Cambiamos la dirección del enemigo al chocar con un elemento del escenario u otro enemigo.
                Flip();
            }
            else if (collision.gameObject.tag == "MARIO" ) {

                //si el jugador a colisionado con el Woompa por la parte superior lo elimina, en caso contrario el jugador pierde la partida.
                if (direction.y < 0)
                {
                    Dead();
                }
                else {
                    gameplayManager.GameOver();
                }
          
            }
        }
    }

     private void OnTriggerEnter2D(Collider2D collision)
     {
        //Obtenemos la dirección de la colisión.
        Vector2 direction = transform.position - collision.transform.position;

         if (collision.gameObject.tag == "WOOMPA_LIMIT" && direction.x != 0)
         {
             //canviamos la direccion del enemigo al chocar con un elemento del escenario
             Flip();
         }
     }


    /// <summary>
    /// Cambiamos la velocidad y dirección de movimiento del personaje para moverlo hacia la derecha.
    /// </summary>
    private void MoveRight()
    {
        myRigidbody.velocity += new Vector2(transform.right.x * linealVelocity, transform.right.y * linealVelocity) * Time.deltaTime;
    }

    /// <summary>
    /// Cambiamos la velocidad y dirección de movimiento del personaje para moverlo hacia la izquierda.
    /// </summary>
    private void MoveLeft()
    {
        myRigidbody.velocity -= new Vector2(transform.right.x * linealVelocity, transform.right.y * linealVelocity) * Time.deltaTime;
    }

    /// <summary>
    /// Eliminamos el enemigo una vez muerto al cabo de medio segundo.
    /// </summary>
    private void  Dead() {
        myAnimator.SetBool("Dead", true);
        gameplayManager.AddScore(100);
        source.PlayOneShot(Resources.Load("Sounds/FX/Squish") as AudioClip);
        Destroy(gameObject, 0.5f);
    }

    /// <summary>
    /// Cambia la variable que controla la dirección del enemigo.
    /// </summary>
    private void Flip() {
        isFacingLeft = !isFacingLeft;
    }

    // Update is called once per frame
    void Update () {

        //Mantiene al enemigo en movimiento en función de la dirección.
        if (isFacingLeft)
        {
            MoveLeft();
        }
        else
        {
            MoveRight();
        }
		
	}
}
