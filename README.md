**Readme PEC 2**

**Resumen**

Segunda práctica de la asignatura de programación 2D en Unity. El objetivo es recrear el primer nivel del juego Super Mario Bross con el objetivo de usar las físicas de elementos 2D del motor Unity.

**Requisitos**

Ejecutar en un dispositivo con teclado y ratón (PC , iOS)

**Descripción**

El objetivo de esta práctica era implementar un juego recreando el primer nivel del juego Super Mario Bross.

En este juego el jugador deberá superar el nivel propuesto superando los obstáculos que van desde elementos que entorpecen el paso, zonas de vacío o enemigos controlados por la CPU.

Para superar el nivel el jugador deberá llegar al final del mismo, la zona de final de nivel está marcada con el mástil de una bandera.

En este juego el jugador puede perder de 3 maneras:

- El jugador toca un enemigo, también llamado Woompa.
- El jugador cae en una zona de vacío.
- El jugador se queda sin tiempo para superar el nivel (actualmente 400 segundos).

Asi mismo, las acciones que tiene el jugador para superar estos obstáculos son:

- Saltar sobre un enemigo, inutilizandolo.
- Saltar sobre obstáculos.

También tiene la posibilidad de recolectar monedas del escenario o golpeando desde la parte inferior los bloques marcados con &quot;?&quot;, aunque actualmente no hay ninguna dinámica que recompense la recolección de monedas. Similar a lo anterior, el jugador cuenta con un marcador de puntos que se incrementa al eliminar enemigos o completar el nivel, actualmente tampoco tiene una dinámica que recompense la obtención de puntos.

Tanto si el jugador gana o pierde el juego, se reiniciará la partida.

**Controles**

**Tecla flecha izquierda:** mueve el personaje (Mario) hacía la izquierda, tanto en el suelo como en aire.

**Tecla flecha derecha:** mueve el personaje hacía la derecha, tanto en el suelo como en el aire.

**Tecla espacio:** hace que el personaje salte.

**Tecla escape:** muestra o oculta el menú de pausa.


**Pantallas de PEC 2**

**Pantalla de juego**

En esta pantalla se desarrolla el juego. Esta pantalla está compuesta de varias partes:

Elementos UI

- Marcador: comienza a 0 y se va incrementando cuando el jugador elimina enemigos o finaliza el nivel.
- Contador monedas: se va incrementando cuando el jugador obtiene monedas del escenario o de los bloques &quot;?&quot;.
- Indicador de tiempo: cuenta atrás para que se acabe la partida (actualmente comienza en 400 segundos).

Elementos del juego

- Mario: personaje que representa las acciones del jugador. Puede moverse y saltar por el escenario.
- Monedas: cada una añade una moneda al contador de monedas.
- Bloques &quot;?&quot;: suelta un ítem al golpear la parte inferior. Actualmente solo dan monedas.
- Bloques de ladrillo: actualmente solo sirven como elemento al que el jugador puede subirse.
- Tuberías: actualmente solo sirven de obstáculos para el jugador.
- Woompas (enemigos): se mueven de izquierda a derecha, si el jugador los golpes muere, ha excepción si lo hace por la parte superior, entonces el enemigo muere y se añaden puntos al marcador.

**Menu de pausa**

Si pulsamos la tecla escape en la pantalla de juego se nos mostrara el menu de pausa. Desde este menú tendremos 2 opciones:

- Reiniciar partida:inicia la partida desde el principio, sin salir de la pantalla de juego.
- Salir del juego: cierra el juego.

Nota: actualmente el menú de pausa solo sirve para acceder a las opciones antes mencionadas. No pausa el desarrollo del juego.



**Implementaciones pendientes**

Aunque no formaban parte de los requisitos, se intentó implementar las siguientes funcionalidades, pero por falta de tiempo no se han podido implementar:

- Sistema de vidas del jugador.
- Implementación del modo Super Mario, que permite sobrevivir una vez al contacto con el enemigo y romper los bloques de ladrillo del escenario.

**Datos importantes**

- Existe un bug pendiente de resolución (Ghost Vertices) en el que el jugador se queda parado en algunas zonas (tuberías, conjunto de bloques) si no lleva la velocidad suficiente.
- Se dispone de un video de gameplay del juego en la carpeta videos del proyecto.
- Se dispone de una introducción al código implementado en la carpeta Assets.
- Se han usado assets obtenidos de la web [https://www.spriters-resource.com/nes/supermariobros/](https://www.spriters-resource.com/nes/supermariobros/)