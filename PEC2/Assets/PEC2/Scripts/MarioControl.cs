﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlador del personaje del jugador. 
/// Gestiona el movimiento, el salto y la muerte del jugador.
/// </summary>
public class MarioControl : MonoBehaviour {

    /// <summary>
    /// Velocidad de movimiento del jugador.
    /// </summary>
    public float linealVelocity = 6;

    /// <summary>
    /// Indicamos a otras clases que se ha finalizado el juego mediante un evento.
    /// </summary>
    public event gameOverDelegate GameOver;
    public delegate void gameOverDelegate();

    /// <summary>
    /// Indicamos a otras clases que se ha alcanzado el final del nivel mediante un evento.
    /// </summary>
    public event finalDelegate FinalLevel;
    public delegate void finalDelegate();

    /// <summary>
    /// Potencia de salto del jugador.
    /// </summary>
    public float jumpForce = 175f;

    /// <summary>
    /// Controlador de las físicas del cuerpo del jugador.
    /// </summary>
    private Rigidbody2D myRigidbody;

    /// <summary>
    /// Controlador de las animaciones del jugador.
    /// </summary>
    private Animator myAnimator;

    /// <summary>
    /// Indica si el jugador se está dirigiendo hacia la derecha. 
    /// Usado para controlar la dirección.
    /// </summary>
    private bool isFacingRight = true;

    /// <summary>
    /// Indica si el jugador está tocando el suelo. 
    /// Usado para movimiento y animaciones.
    /// </summary>
    private bool isOnGround = true;

    /// <summary>
    /// Radio de detección de colisiones del jugador.
    /// </summary>
    private float colliderRadius;

    /// <summary>
    /// Indica si el jugador esta muerto.
    /// </summary>
    private bool isDead = false;

    /// <summary>
    /// Conponente de los sonidos efectuados por el jugador.
    /// </summary>
    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();

        myAnimator.SetBool("Ground", true);

        colliderRadius =  GetComponent<BoxCollider2D>().size.y/2;

        //evita rotaciones incesarias
        myRigidbody.freezeRotation = true;

        source = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Cambiamos la velocidad y dirección de movimiento del personaje para moverlo hacia la derecha.
    /// </summary>
    private void MoveRight()
    {
            myRigidbody.velocity += new Vector2(transform.right.x * linealVelocity, transform.right.y * linealVelocity) * Time.deltaTime;
            print("right: "+ myRigidbody.velocity);
    }

    /// <summary>
    /// Cambiamos la velocidad y dirección de movimiento del personaje para moverlo hacia la izquierda.
    /// </summary>
    private void MoveLeft()
    {
            myRigidbody.velocity -= new Vector2(transform.right.x * linealVelocity, transform.right.y * linealVelocity) * Time.deltaTime;
            print("left: " + myRigidbody.velocity);
    }

    /// <summary>
    /// Cambiamos la posición vertical para efectuar un salto
    /// </summary>
    public void Jump()
    {
        myAnimator.SetBool("Ground", false);
        myRigidbody.AddForce(new Vector2(0, jumpForce));
        source.PlayOneShot(Resources.Load("Sounds/FX/Jump") as AudioClip);
    }

    /// <summary>
    /// Gira el sprite en función del lugar donde se mueva el jugador
    /// </summary>
    private void Flip() {
        isFacingRight = !isFacingRight;
        Vector3 myScale = transform.localScale;
        //invertimos el sprite sobre su eje x
        myScale.x *= -1;
        transform.localScale = myScale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject != gameObject)
        {
            
            if (!isDead) { 

             if (collision.gameObject.tag == "WOOMPA")
                {
                    //Obtenemos la dirección de la colisión.
                    Vector2 direction = transform.position - collision.transform.position;

                    //si el jugador a colisionado con el Woompa por la parte superior se realiza un salto
                    if (direction.y > 0)
                    {
                        Jump();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Creamos un círculo alrededor de nuestro personaje 
    /// un poco más grande que esta, y miramos si colisiona con algo más.
    /// </summary>
    /// <returns>El perosnaje esta en el suelo o no</returns>
    bool OnGround()
    {
        return (Physics2D.OverlapCircleAll(myRigidbody.position, colliderRadius * 1.3f).Length > 1); 
    }

    public void Dead() {
        isDead = true;
        myAnimator.SetBool("Dead", true);
        myRigidbody.AddForce(new Vector2(0, 200f));
        gameObject.tag = "MARIO_DEAD";

    }

    // Update is called once per frame
    void Update () {

        if (!isDead) {

            //controles de movimiento
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }

            if (isOnGround && Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }

            //se conprueba la direccion actual para canviar la orientacion del sprite
            if (Input.GetAxis("Horizontal") > 0 && !isFacingRight)
            {
                Flip();
            }
            else if (Input.GetAxis("Horizontal") < 0 && isFacingRight)
            {
                Flip();
            }

        }

    }

    private void FixedUpdate()
    {
        //comprobamos que el jugador está en el suelo.
        isOnGround = OnGround();

        //controles de animación
        myAnimator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));

        myAnimator.SetBool("Ground", isOnGround);
    }
}
