﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlador de la cámara que permite seguir al jugador por el escenario.
/// </summary>
public class MarioFollower : MonoBehaviour {

    /// <summary>
    /// Posición actual del jugador
    /// </summary>
    public Transform mario;

    /// <summary>
    /// Diferencia entre la posición actual y el jugador.
    /// </summary>
    Vector3 offset;

    // Use this for initialization
    void Start () {
        offset = mario.transform.position - transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = mario.position - offset;
    }
}
