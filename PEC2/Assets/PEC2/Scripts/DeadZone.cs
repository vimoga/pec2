﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlador de las zonas muertas del juego (saltos al vacío).
/// </summary>
public class DeadZone : MonoBehaviour {

    /// <summary>
    /// Controlador general del juego. Usado para gestionar la partida.
    /// </summary>
    public GameplayManager gameplayManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si Mario entre en la zona muerta se acaba el juego.
        if (collision.tag == "MARIO")
            gameplayManager.GameOver();
    }

}
