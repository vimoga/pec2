**Readme Codigo  PEC 2**

A continuación de detalla de una forma simplificada como funciona el código de las diferentes scenes, scripts y otros elementos de la PEC2

Se tomará como referencia las pantallas principales del juego.


**Pantalla de juego**

**Scene Mario1-1:**

Scene donde se desarrollará el juego, está compuesta por los siguientes apartados, implementados por diferentes scripts:

Elementos UI

Dan la información necesaria al jugador sobre el desarrollo de la partida, estos elementos están controlados por el script GameplayManager:

- **Marcador:** comienza a 0 y se va incrementando cuando el jugador elimina enemigos (añade 100 puntos) o finaliza el nivel (añade 5000 puntos).
- **Contador monedas** : se va incrementando cuando el jugador obtiene monedas del escenario o de los bloques &quot;?&quot;. Se añaden una vez que el script CoinManager o el script BlockManager han detectado una colisión valida con el jugador.
- **Indicador de tiempo:** cuenta atrás para que se acabe la partida (actualmente comienza en 400 segundos). Controlado por la función Update del script.

Elementos del juego

Elementos que componen la partida, desde el avatar del jugador a los enemigos pasando por el escenario.

- **Mario:** personaje que representa las acciones del jugador. Puede moverse y saltar por el escenario. Sus acciones vienen dadas y controladas por el script MarioControl. Está definido por componentes tales como un Sprite que permite la visualización, un BoxColider2D que permite detectar las colisiones con el entorno, un RigyBody2D que controla el comportamiento de las físicas y otros componentes como un Animator o un AudioSource para gestionar las animaciones y sonidos del elemento.
- **Woompas (enemigos):** se mueven de izquierda a derecha, si el jugador los golpea muere, a excepción de si lo hace por la parte superior, entonces el enemigo muere y se añaden puntos al marcador. Está controlado por la CPU. Su comportamiento está gestionado por el script WoompaManager. Los componentes que lo definen son los mismos que en el elemento Mario.
- **Monedas** : cada una añade una moneda al contador de monedas. Están asociadas al script CoinManager y están compuestas por un Sprite y un BoxColider2D.
- **Bloques &quot;?&quot;:** suelta un ítem al golpear la parte inferior. Actualmente solo dan monedas. Están asociadas al script BlockManager y lo componen los mismos elementos que las monedas.
- **Bloques de ladrillo y Tuberías:** actualmente solo sirven de obstáculos o como elemento al que el jugador puede subirse. Se component simplemente se Sprites y BoxColider2D.

Otros elementos

Elementos necesarios para ejecución óptima de la partida.

- **Límites izquierdo y derecho:** son simples BoxColider2D que sirven de muros invisibles, evitan que el jugador salga del escenario del juego.
- **Limites de Woompa:** también son  BoxColider2D pero estos funcionan vía Trigger. Solo  lo pueden activar los Woompas y evitan que caigan al vacío (la implementación de esto está en el script WoompaManager).
- **DeadZones:** son las zonas donde el jugador puede caer al vacío. Al igual que el apartado anterior son BoxColider2D que funcionan vía Trigger.
- **BackgroundMusic:** AudioSource que reproduce la música del juego.
- **MainCamera:** seguirá al jugador mediante el script MarioFollower

**Script MarioControl**

Se encarga de recibir los inputs del usuario y traducirlos en movimiento del personaje del jugador. Actualmente el jugador solo puede moverse hacia los lados (esto se consigue añadiendo velocidad al elemento Mario) y saltar (aplicando una fuerza al elemento Mario).

Además comprueba que el personaje se encuentre en el suelo o en aire mediante una comprobación de su radio de colisión, con el objetivo de controlar correctamente el movimiento.

También se encarga de rebotar si ha colisionado con la parte superior de un Woompa (esto se consigue mediante el gestor de colisiones implementado en Unity). Entre sus funciones también se encuentra gestionar las animaciones y sonidos del elemento, entre ellas la muerte del personaje. La muerte vendrá indicada por el script GameplayManager mediante un evento.

**Script WoompaManager**

Se encarga de implementar el movimiento y comportamiento de los enemigos del juego. Respecto al movimiento, los enemigos solo se mueven de izquierda a derecha. Esto se hace añadiendo velocidad al elemento en cada frame y se cambia la dirección si se ha colisionado con un elemento del escenario o se ha detectado una colisión con un límite de Woompa. También se detectan las colisiones con el jugador, si la colisión se ha efectuado por la parte superior, el enemigo ejecuta la animación correspondiente de su muerte, se envía un evento al script GameplayManager para que añada puntos al jugador y se elimina el objeto de la scene. En caso contrario se notifica al script GameplayManager para que ejecute el fin de la partida.

También gestiona las animaciones y sonidos del enemigo.

**Script BlockManager**

Gestiona el comportamiento de los bloques &quot;?&quot;. Comprueba si el jugador a colisionado por la parte inferior con un bloque &quot;?&quot;. En caso afirmativo, se ejecuta una animación que simula que sale una moneda y se notifica al script GameplayManager para que añada una moneda al contador. Posteriormente el bloque queda inutilizado.

**Script CoinManager**

Gestiona el comportamiento de las monedas del escenario. El funcionamiento es similar al apartado anterior. Si el jugador colisiona con el elemento se inicia el proceso para añadir una moneda al contador y hacerla desaparecer de la scene.

**Script DeadZone**

Gestiona la detección de las zonas de salto al vacío. Si el jugador entra en ellas se indica al script GameplayManager que acabe la partida.

**Script LevelEnd**

Gestiona la detección de la zonas de fin de nivel. Si el jugador entra en ella se indica al script GameplayManager que acabe la partida dando la victoria al jugador..

**Script MarioFollower**

Comprueba la diferencia entre la posición de la cámara y el personaje del jugador para modificar la cámara y que siga al jugador por el escenario.

**Script GameplayManager**

Recibe los inputs del resto de elementos de la partida para poder gestionarla en función de ellos. Si se recibe notificación de los scripts CoinManager o BlockManager de que el usuario a obtenido una moneda se incrementara el contador de monedas. En caso de que lo hagan los scripts DeadZone o LevelEnd se finalizará la partida o se dará por ganada respectivamente.

En caso de que el script WoompaManager indique que el jugador ha eliminado a un enemigo se incrementara el marcador de puntos. En caso de que sea el enemigo el que mata al jugador, el script GameplayManager finaliza la partida e indicará al script MarioController que ejecute la animación de muerte (esto también ocurre si recibe notificación del script DeadZOne).

Este script también gestiona la música del juego y la modifica en función de la situación. Tanto si el jugador gana o pierde el juego, el script reiniciará la partida.

**Menu de pausa:**

**Script PauseScreenManager**

En este código se define el comportamiento del juego cuando el usuario pulse la tecla &quot;escape&quot;. En el caso de que el usuario presione la tecla correspondiente, se mostrará o se ocultará el menú de pausa.

También se definen las funciones que ejecutan las opciones que tiene el usuario: reiniciar partida y salir del juego.

En el caso de reiniciar la partida, se volverá a abrir la scene Mario1-1, de ese modo, de cara al usuario se reiniciará el juego.

**Game Object Pause Screen**

Incorpora botones enlazadas a sus respectivas funciones en el script PauseScreenManager. Las opciones (botones) del usuario en este menú son: reiniciar partida y salir del juego.

Como funcionalidad específica, este menú se le mostrará al usuario al pulsar la tecla &quot;escape&quot;.

**Implementaciones pendientes:**

Aunque no formaban parte de los requisitos, se intentó implementar las siguientes funcionalidades, pero por falta de tiempo no se han podido implementar:

- Sistema de vidas del jugador.
- Implementación del modo Super Mario, que permite sobrevivir una vez al contacto con el enemigo y romper los bloques de ladrillo del escenario.
- Existe un bug pendiente de resolución (Ghost Vertices) en el que el jugador se queda parado en algunas zonas (tuberías, conjunto de bloques) si no lleva la velocidad suficiente.

**Datos importantes:**

- Muchos elementos y código de esta práctica se han realizado a partir de lo que se proporcionó como template de este proyecto.