﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlador de las monedas. Usado para controlar la detección de colisión.
/// </summary>
public class CoinManager : MonoBehaviour {

    /// <summary>
    /// Controlador general del juego. Usado para gestionar la partida.
    /// </summary>
    public GameplayManager gameplayManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
   
        if (collision.gameObject != gameObject)
        {
            if (collision.gameObject.tag == "MARIO")
            {
                //añadimos la moneda al contador y la eliminamos del juego
                gameplayManager.AddCoin();
                Destroy(gameObject);
            }
        }
    }

}
