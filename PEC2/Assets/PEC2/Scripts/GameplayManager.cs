﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

/// <summary>
/// Controlador general del juego. Usado para gestionar la partida como la
/// finalización de partida, completar el nivel o gestionar los puntos o 
/// los objetos recolectados.
/// </summary>
public class GameplayManager: MonoBehaviour {

    /// <summary>
    /// Controlador del personaje del jugador.
    /// </summary>
    public MarioControl marioController;

    /// <summary>
    /// Texto de la UI que refleja la puntuación actual.
    /// </summary>
    public Text scoreText;

    /// <summary>
    /// Texto de la UI que refleja las monedas recolectadas del jugador.
    /// </summary>
    public Text coinText;

    /// <summary>
    /// Texto de la UI que refleja el tiempo restante para finalizar la partida.
    /// </summary>
    public Text timeText;

    /// <summary>
    /// Texto de la UI que indica que la partida a finalizado.
    /// </summary>
    public Text gameOverText;

    /// <summary>
    /// Indica si tenemos que seguir descontando tiempo límite.
    /// </summary>
    private bool updateTime;

    /// <summary>
    /// Tiempo total de la partida.
    /// </summary>
    private int timeInMillis = 24000;

    /// <summary>
    /// Indica si el nivel esta completado.
    /// </summary>
    private bool isLevelComplete;

    /// <summary>
    /// Componente de la música de fondo que se escucha durante la partida.
    /// </summary>
    private AudioSource backgroundMusic;

    /// <summary>
    /// Componente de los sonidos que se escucharan durante la partida.
    /// </summary>
    private AudioSource source;

    // Use this for initialization
    void Start () {
        gameOverText.enabled = false;
        updateTime = true;
        isLevelComplete = false;
        marioController.GameOver += GameOver;
        marioController.FinalLevel += EndLevel;
        source = GetComponent<AudioSource>();
        backgroundMusic = GameObject.Find("BackgroundMusic").GetComponent<AudioSource>();
    }

    /// <summary>
    /// Reinicia la partida.
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Finaliza la partida y gestiona su comportamiento (mostrar Game Over, 
    /// matar al personaje del jugador, cambiar música, reiniciar la partida ...).
    /// </summary>
    public void GameOver() {
        gameOverText.enabled = true;
        updateTime = false;
        marioController.Dead();
        backgroundMusic.Stop();
        source.PlayOneShot(Resources.Load("Sounds/FX/Die") as AudioClip);
        Invoke("Restart", 3f);
    }

    /// <summary>
    /// Añade una moneda al contador y reproduce el sonido.
    /// </summary>
    public void AddCoin()
    {
        int coins = Convert.ToInt32(coinText.text);
        coins += 1;
        coinText.text = coins.ToString();
        source.PlayOneShot(Resources.Load("Sounds/FX/Coin") as AudioClip);
    }

    /// <summary>
    /// Añade la puntuación introducida al marcador.
    /// </summary>
    /// <param name="scoreToAdd">puntos a añadir al marcador</param>
    public void AddScore(int scoreToAdd)
    {
        int score = Convert.ToInt32(scoreText.text);
        score += scoreToAdd;
        scoreText.text = score.ToString();
    }

    /// <summary>
    /// Finaliza la partida cuando el jugador llega al final del nivel. 
    /// Gestiona su comportamiento (mostrar mensaje de nivel completado, 
    /// cambiar música, reiniciar la partida ...).
    /// </summary>
    public void EndLevel()
    {
        if (!isLevelComplete)
        {
            AddScore(5000);
            updateTime = false;
            //reutilizamos el texto de Game Over para indicar que se ha completado el nivel.
            gameOverText.text = "Level complete!";
            gameOverText.enabled = true;
            backgroundMusic.Stop();
            source.PlayOneShot(Resources.Load("Sounds/Music/level_complete") as AudioClip);
            Invoke("Restart", 7f);
            isLevelComplete = true;
        }

    }

    // Update is called once per frame
    void Update () {

        //reducimos el tiempo límite para completar el nivel.
        if (updateTime) {
            
            timeInMillis -= 1;
            
            timeText.text = (timeInMillis / 60).ToString();
            if (timeInMillis <= 0)
            {
                //finalizamos la partida si llega al tiempo límite.
                GameOver();
            }
        }
	}
}
